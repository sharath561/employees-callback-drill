const fs = require("fs");
// to read the file
function readFile(file, callback) {
  fs.readFile(file, "utf-8", (error, data) => {
    if (error) {
      console.log(error);
    } else {
      callback(null, data);
    }
  });
}
// to write the file
function writeFile(path, employeesData, callback) {
  fs.writeFile(path, JSON.stringify(employeesData), (error) => {
    if (error) {
      console.log(error);
    } else {
      callback(null, `Data is written in ${path} file suuccessfully`);
    }
  });
}

//1. Retrieve data for ids : [2, 13, 23].
function retiveData(employeesData, callback) {
  try {
    if (typeof employeesData == "object" && callback != undefined) {
      let storeData = employeesData.filter((current) => {
        if (current.id == 2 || current.id == 13 || current.id == 23) {
          return current;
        }
      });
      callback(null, storeData);
    } else {
      throw new Error("error : Data is not exported correctly");
    }
  } catch (error) {
    //console.log(error.message)
    callback(error.message);
  }
}

// 2. Group data based on companies.{ "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
function groupByCompany(employeesData, callback) {
  try {
    if (typeof employeesData == "object" && callback !== undefined) {
      let storeCompanyData = employeesData.reduce((accumulator, current) => {
        if (accumulator[current.company]) {
          accumulator[current.company].push(current);
        } else {
          accumulator[current.company] = [];
          accumulator[current.company].push(current);
        }

        return accumulator;
      }, {});

      callback(null, storeCompanyData);
    } else {
      throw new Error("Data is not exported correctly");
    }
  } catch (error) {
    callback(error.message);
  }
}

//3.Get all data for company Powerpuff Brigade
function companyPowerPuffBridge(employeesData, callback) {
  try {
    if (typeof employeesData == "object" && callback !== undefined) {
      let storePowerPuffData = employeesData.filter((current) => {
        if (current.company == "Powerpuff Brigade") {
          return current;
        }
      });
      callback(null, storePowerPuffData);
    } else {
      throw new Error("Data is not exported correctly");
    }
  } catch (error) {
    callback(error.message);
  }
}

//4. Remove entry with id 2.
function removeId2(employeesData, callback) {
  try {
    if (typeof employeesData == "object" && callback !== undefined) {
      let removedData = employeesData.filter((current) => {
        if (current.id !== 2) {
          return current;
        }
      });
      callback(null, removedData);
    } else {
      throw new Error("Data is not exported correctly");
    }
  } catch (error) {
    callback(error.message);
  }
}

//5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
function sortByCompanyName(employeesData, callback) {
  try {
    if (typeof employeesData == "object" && callback !== undefined) {
      let sortedData = employeesData.sort((company1, company2) => {
        if (company1.company < company2.company) {
          return -1;
        } else if (company1.company == company2.company) {
          if (company1.id < company2.id) {
            return -1;
          } else {
            return 1;
          }
        } else {
          return 1;
        }
      });
      callback(null, sortedData);
    } else {
      throw new Error("Data is not exported correctly");
    }
  } catch (error) {
    callback(error.message);
  }
}

//6. Swap position of companies with id 93 and id 92.

function swapPosition93And92(employeesData, callback) {
  try {
    if (typeof employeesData == "object" && callback !== undefined) {
      let Data92;
      let Data93;

      employeesData.forEach((current, index) => {
        if (current.id == 92) {
          Data92 = index;
        } else if (current.id == 93) {
          Data93 = index;
        }
      });

      let swapData = employeesData[Data92];
      employeesData[Data92] = employeesData[Data93];
      employeesData[Data93] = swapData;

      callback(null, employeesData);
    } else {
      throw new Error("Data is not exported correctly");
    }
  } catch (error) {
    callback(error.message);
  }
}

//7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.
function oddIdDate(employeesData, callback) {
  try {
    if (typeof employeesData == "object" && callback !== undefined) {
      const today = new Date();

      employeesData.forEach((current) => {
        if (current.id % 2 == 0) {
          current["Date"] =
            today.getDate() +
            "-" +
            today.getMonth() +
            "-" +
            today.getFullYear();
        }
      });

      callback(null, employeesData);
    } else {
      throw new Error("Data is not exported correctly");
    }
  } catch (error) {
    callback(error.message);
  }
}


function mainFile() {
  readFile("./data.json", (error, data) => {
    if (error) {
      console.log(error);
    } else {
      modifyData = JSON.parse(data);

      //1. Retrieve data for ids : [2, 13, 23].
      retiveData(modifyData.employees, (error, data) => {
        if (error) {
          console.log(error);
        } else {
          let retrieveOutputpath = "./output/1-retriveDataBasedOnIds.json";
          writeFile(retrieveOutputpath, data, (error, message) => {
            if (error) {
              console.log(error);
            } else {
              console.log(message);
              // 2. Group data based on companies.{ "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}

              groupByCompany(modifyData.employees, (error, data) => {
                if (error) {
                  console.log(error);
                } else {
                  let storeCompanyData = "./output/2-groupByCompaniesData.json";
                  writeFile(storeCompanyData, data, (error, message) => {
                    if (error) {
                      console.log(error);
                    } else {
                      console.log(message);

                      //3.Get all data for company Powerpuff Brigade
                      companyPowerPuffBridge(
                        modifyData.employees,
                        (error, data) => {
                          if (error) {
                            console.log(error);
                          } else {
                            let PowerpuffCompanyData =
                              "./output/3-powerPuffBridgeCompanyData.json";
                            writeFile(
                              PowerpuffCompanyData,
                              data,
                              (error, message) => {
                                if (error) {
                                  console.log(error);
                                } else {
                                  console.log(message);

                                  //4. Remove entry with id 2.
                                  removeId2(
                                    modifyData.employees,
                                    (error, data) => {
                                      if (error) {
                                        console.log(error);
                                      } else {
                                        let removedPath =
                                          "./output/4-removedId2.json";
                                        writeFile(
                                          removedPath,
                                          data,
                                          (error, message) => {
                                            if (error) {
                                              console.log(error);
                                            } else {
                                              //console.log(message);

                                              //5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
                                              const newData = JSON.parse(
                                                JSON.stringify(modifyData)
                                              );
                                              //console.log(newData)
                                              sortByCompanyName(
                                                newData.employees,
                                                (error, data) => {
                                                  if (error) {
                                                    console.log(error);
                                                  } else {
                                                    //console.log(data);
                                                    let sortedDataPath =
                                                      "./output/5-sortedByCompaniesData.json";
                                                    writeFile(
                                                      sortedDataPath,
                                                      data,
                                                      (error, message) => {
                                                        if (error) {
                                                          console.log(error);
                                                        } else {
                                                          console.log(message);

                                                          //6. Swap position of companies with id 93 and id 92.

                                                          swapPosition93And92(
                                                            modifyData.employees,
                                                            (error, data) => {
                                                              if (error) {
                                                                console.log(
                                                                  error
                                                                );
                                                              } else {
                                                                //console.log(data)
                                                                let swap92And93Path =
                                                                  "./output/6-swapDataOf92And93Data.json";
                                                                writeFile(
                                                                  swap92And93Path,
                                                                  data,
                                                                  (
                                                                    error,
                                                                    message
                                                                  ) => {
                                                                    if (error) {
                                                                      console.log(
                                                                        error
                                                                      );
                                                                    } else {
                                                                      console.log(
                                                                        message
                                                                      );

                                                                      //7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

                                                                      oddIdDate(
                                                                        modifyData.employees,
                                                                        (
                                                                          error,
                                                                          data
                                                                        ) => {
                                                                          if (
                                                                            error
                                                                          ) {
                                                                            console.log(
                                                                              error
                                                                            );
                                                                          } else {
                                                                            let datePath =
                                                                              "./output/7-addingDatesOddId.json";

                                                                            writeFile(
                                                                              datePath,
                                                                              data,
                                                                              (
                                                                                error,
                                                                                message
                                                                              ) => {
                                                                                if (
                                                                                  error
                                                                                ) {
                                                                                  console.log(
                                                                                    error
                                                                                  );
                                                                                } else {
                                                                                  console.log(
                                                                                    message
                                                                                  );
                                                                                }
                                                                              }
                                                                            );
                                                                          }
                                                                        }
                                                                      );
                                                                    }
                                                                  }
                                                                );
                                                              }
                                                            }
                                                          );
                                                        }
                                                      }
                                                    );
                                                  }
                                                }
                                              );
                                            }
                                          }
                                        );
                                      }
                                    }
                                  );
                                }
                              }
                            );
                          }
                        }
                      );
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}
mainFile();
